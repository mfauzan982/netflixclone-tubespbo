# No 1

**Use Case Sisi User**
| No | Use Case | Prioritas | Status |
|----|----------|-----------|--------|
|1|Pengguna dapat membuka aplikasi Netflix|Tinggi|Completed|
|2|	Pengguna dapat mengakses konten Netflix melalui berbagai platform, termasuk smartphone, tablet, Smart TV, dan komputer.|Sedang|Completed
|3|Aplikasi menampilkan halaman login.|Tinggi|Completed|
|4|Jika tidak memiliki akun, pengguna dapat membuat akun terlebih dahulu.|Tinggi|Completed|
|5|Jika pengguna memiliki pertanyaan ketika login, pengguna dapat mengakses fitur “Need help”.|Sedang|Uncompleted
|6|Pengguna memasukkan username dan password mereka.|Tinggi|Completed|
|7|Pengguna dapat menggunakan fitur forget password jika lupa password.|Sedang|Unclompeted|
|8|Jika login berhasil, aplikasi menampilkan halaman utama atau dashboard.|Tinggi|Completed
|9|Jika memiliki paket Premium pengguna dapat memiliki hingga 5 profile untuk 1 akun netflix mereka.|Sedang|Completed|
|10|Pengguna dapat berpindah-pindah profile.|Sedang|Unclompeted|
|11|Pengguna dapat mengunci tiap profile dengan 4 angka pin yang mereka tetapkan.|Sedang|Unclompeted|
|12|Pengguna dapat mengatur profile picture tiap profile dengan berbagai macam pilihan yang disediakan.|Rendah|Unclompeted|
|13|Pengguna dapat memilih bahasa yang digunakan untuk dashboard netflix.|Rendah|Unclompeted|
|14|Pengguna dapat melihat daftar film dan serial TV yang tersedia.|Tinggi|Unclompeted|
|15|Pengguna dapat mencari film atau serial TV berdasarkan judul atau kategori.|Sedang|Unclompeted|
|16|Pengguna dapat melihat detail film atau serial TV tertentu, termasuk judul, tahun rilis, durasi, sinopsis, dan rating.|Sedang|Completed
|17|Pengguna dapat menambahkan film atau serial TV ke daftar tontonan atau favorit.|Sedang|Completed|
|18|Pengguna dapat menonton film atau serial TV yang dipilih.|Tinggi|Unclompeted
|19|Menandai film/serial TV sebagai "Favorit" untuk referensi cepat di masa mendatang.|Sedang|Unclompeted|
|20|Menyembunyikan judul film/serial TV dari rekomendasi dengan fitur "Sembunyikan dari Rekomendasi".|Rendah|Unclompeted|
|21|Jika pengguna memilih menonton, aplikasi menampilkan halaman pemutaran.|Tinggi|Unclompeted|
|22|Pengguna dapat memutar, menjeda, atau menghentikan pemutaran.|Sedang|Unclompeted|
|23|Pengguna dapat menonton film/serial TV dengan kualitas streaming yang tertinggi jika memiliki paket 4K+.|Sedang|Unclompeted|
|24|Menggunakan fitur "Continue Watching" untuk melanjutkan menonton di perangkat yang berbeda dengan mudah.|Sedang|Unclompeted|
|25|Pengguna dapat melanjutkan menonton dari titik terakhir yang ditinggalkan.|Sedang|Unclompeted|
|26|Pengguna dapat mengunduh film/serial TV untuk ditonton secara offline.|Sedang|Unclompeted|
|27|Memberikan rating dan ulasan untuk film/serial TV yang ditonton.|Rendah|Unclompeted|
|28|Mengatur preferensi konten untuk mendapatkan rekomendasi yang lebih sesuai.|Rendah|Unclompeted
|29|Mengatur pengaturan kontrol orang tua untuk membatasi akses anak-anak ke konten yang tidak sesuai.|Rendah|Unclompeted|
|30|Mengatur profil pengguna terpisah dengan preferensi dan rekomendasi yang dipersonalisasi.|Rendah|Unclompeted|
|31|Menonton trailer dan cuplikan untuk mendapatkan gambaran tentang konten yang akan ditonton.|Rendah|Completed|
|32|Menonton film/serial TV dalam mode "Maraton" secara berurutan tanpa interupsi.|Rendah|Unclompeted
|33|Menggunakan fitur "Skip Intro" untuk melewati bagian intro yang berulang di serial TV.|Rendah|Unclompeted|
|34|Menggunakan fitur "Skip Recap": Melewati bagian pengulangan adegan sebelum episode baru dalam serial TV yang sedang ditonton.|Rendah|Unclompeted
|35|Menonton konten Netflix dengan audio deskripsi untuk pengalaman menonton yang inklusif bagi pengguna dengan kebutuhan visual.|Sedang|Unclompeted
|36|Jika pengguna memiliki paket tertentu, pengguna dapat menonton film/serial TV dengan audio multi-channel atau Dolby Atmos untuk pengalaman suara yang lebih mendalam.|Tinggi|Unclompeted|
|37|Pengguna dapat mengatur pengaturan "Data Usage" untuk mengontrol penggunaan data saat menonton di perangkat seluler.|Sedang|Unclompeted
|38|Setelah menonton selesai, pengguna dapat menutup halaman pemutaran dan kembali ke halaman utama.|Sedang|Unclompeted|
|39|Menggunakan fitur "Actor/Actress Filmography" untuk menjelajahi filmografi dari aktor/aktris yang terlibat dalam film/serial TV tertentu.|Rendah|Unclompeted
|40|Menggunakan fitur "Smart Recommendations" untuk mendapatkan rekomendasi konten yang disesuaikan berdasarkan tren saat ini dan popularitas di kalangan pengguna Netflix lainnya.|Rendah|Unclompeted
|41|Pengguna dapat mengakses setting aplikasi untuk menyesuaikan kebutuhan.|Rendah|Unclompeted|
|42|Pengguna dapat logout dari aplikasi.|Sedang|Unclompeted
|43|Pengguna memilih lalu membeli paket yang tersedia|Tinggi|Completed
|44|Pengguna dapat menentukan metode pembayaran|Sedang|Completed
|45|Pengguna dapat melakukan upgrade paket dari yang dimilikinya|Sedang|Completed
|46|Pengguna dapat menghentikan/membatalkan langganan mereka|Sedang|Completed


**Use Case Sisi Manajemen**
| No | Use Case | Prioritas | Status |
|----|----------|-----------|--------|
|1|Admin Netflix mengatur pembaruan konten.|Tinggi|Unclompeted
|2|Netflix mengatur jadwal tayangan untuk film dan serial TV yang mereka miliki, termasuk penentuan tanggal rilis dan pengaturan tayangan berdasarkan geografi atau wilayah tertentu.|Tinggi|Unclompeted
|3|Netflix terus mengembangkan dan memperbarui fitur-fitur platform mereka, seperti mode tontonan offline, kontrol parental, dan fitur interaktif, untuk meningkatkan pengalaman pengguna.|Tinggi|Unclompeted
|4|Admin melindungi privasi pengguna dan mengelola keamanan data.|Tinggi|Completed
|5|Admin Netflix dapat mengelola akun pengguna, termasuk membuat, menghapus, dan mengubah informasi akun.|Sedang|Completed
|6|Manajemen dapat membuat paket-paket baru yang dapat menarik minat pengguna.|Sedang|Completed
|7|Manajemen dapat menyesusaikan harga dan fitur paket yang telah ada|Sedang|Completed
|8|Admin dapat mengelola metode pembayaran yang tersedia bagi pengguna, memastikan kelancaran proses pembayaran dan pembaruan langganan.|Sedang|Completed
|9|Admin Netflix mengelola ketersediaan subtitle dan dubbing untuk berbagai konten, termasuk memastikan kualitas terjemahan dan akurasi subtitle.|Sedang|Unclompeted
|10|Manjemen Netflix menyusun laporan keuangan, analisis anggaran, dan melacak pendapatan serta biaya yang terkait dengan operasional perusahaan.|Rendah|Unclompeted
|11|Admin mengelola kebijakan refund dan mengurus klaim pelanggan terkait masalah pembayaran, gangguan layanan, atau masalah lainnya..|Rendah|Unclompeted
|12|Admin dapat mengelola konten yang ditampilkan di platform Netflix, termasuk menambahkan, menghapus, atau memperbarui konten film, serial TV, dan program lainnya.|Sedang|Unclompeted

**Use Case Sisi Direksi**
| No | Use Case | Prioritas | 
|----|----------|-----------|
|1|Direksi netflix mengelola lisensi konten yang dimiliki oleh perusahaan, termasuk memantau status lisensi, memperbarui perjanjian|Tinggi|
|2|Direksi netflix dapat menggunakan dashboard untuk memantau kinerja keuangan secara real-time, melihat pendapatan, pengeluaran, laba, dan metrik keuangan lainnya.|Tinggi|
|3|Direksi dapat menggunakan dashboard analitik untuk menganalisis data pengguna, termasuk tren penonton, preferensi konten, durasi tayangan, dan perilaku pengguna lainnya.|Sedang|
|4|Direksi dapat menggunakan dashboard untuk memantau pertumbuhan pelanggan, mengamati peningkatan atau penurunan jumlah pelanggan, dan menganalisis faktor-faktor yang memengaruhi pertumbuhan tersebut.|Rendah|
|5|Direksi dapat menggunakan dashboard untuk memantau ketersediaan konten, termasuk jumlah konten yang ditawarkan, keberhasilan lisensi, dan pembaruan konten baru.|Rendah|
|6|Direksi dapat menggunakan dashboard analitik untuk menganalisis persaingan dengan platform streaming lainnya, melihat peringkat, pertumbuhan pengguna, dan strategi bisnis pesaing.|Tinggi|
|7|Direksi dapat menggunakan dashboard untuk menganalisis penggunaan platform Netflix, termasuk durasi tayangan, frekuensi penggunaan, dan interaksi pengguna dengan fitur-fitur platform.|Sedang|
|8|Direksi dapat menggunakan dashboard untuk menganalisis ROI dari investasi perusahaan, termasuk investasi dalam konten, teknologi, pemasaran, dan strategi bisnis lainnya.|Tinggi|
|9| Direksi dapat menggunakan dashboard untuk menganalisis kepuasan pengguna terhadap fitur dan fungsionalitas platform Netflix, melihat umpan balik pengguna, evaluasi penggunaan fitur, dan meningkatkan pengalaman pengguna secara keseluruhan.|Tinggi|
|10|Direksi dapat menggunakan dashboard untuk menganalisis penyebaran dan penerimaan fitur baru oleh pengguna, melihat tingkat adopsi, umpan balik pengguna, dan memahami dampak fitur baru pada pertumbuhan dan retensi pelanggan.|Tinggi|

# No 2
**Class Diagram**

![](https://gitlab.com/mfauzan982/projecttubes-prakpbo/-/raw/master/ScreenShoot%20Jawaban/Class_diagram_netlix.drawio.png)

# No 3
**SOLID Design Principle**

1. Single Responsibility Principle (Prinsip Tanggung Jawab Tunggal): Setiap kelas atau modul harus memiliki satu tanggung jawab tunggal dan hanya bertanggung jawab terhadap satu aspek atau fungsi dalam sistem.
2. Open-Closed Principle (Prinsip Terbuka-Tertutup): Modul atau entitas perangkat lunak harus terbuka untuk perluasan (open for extension) namun tertutup untuk modifikasi (closed for modification). Dalam arti lain, Anda harus dapat memperluas fungsionalitas entitas tanpa mengubah kode yang sudah ada.
3. Liskov Substitution Principle (Prinsip Substitusi Liskov): Subkelas harus dapat digunakan sebagai pengganti kelas induk tanpa mempengaruhi kebenaran atau konsistensi sistem. Dalam hal ini, subkelas harus mengikuti kontrak dan prasyarat yang didefinisikan oleh kelas induk.
4. Interface Segregation Principle (Prinsip Segregasi Antarmuka): Klien tidak boleh dipaksa untuk bergantung pada antarmuka yang tidak mereka gunakan. Prinsip ini mendorong pemisahan antarmuka menjadi bagian-bagian yang lebih kecil dan khusus, sehingga klien hanya menggunakan antarmuka yang relevan bagi mereka.
5. Dependency Inversion Principle (Prinsip Inversi Ketergantungan): Modul-level tinggi tidak boleh bergantung pada modul-level rendah. Keduanya seharusnya bergantung pada abstraksi. Prinsip ini mendorong penggunaan injeksi ketergantungan (dependency injection) dan penggunaan antarmuka atau kelas abstrak untuk mengurangi ketergantungan langsung pada implementasi konkret.

- **Single Responsibility Principle (Prinsip Tanggung Jawab Tunggal)**

Komponen Home bertanggung jawab untuk merender halaman utama dengan beberapa komponen seperti Header, Banner, dan CategoryRow

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/3-1.gif)

- **Open-Closed Principle (Prinsip Terbuka-Tertutup)**

Komponen AuthProvider dapat diperluas dengan menambahkan lebih banyak fungsi terkait autentikasi, seperti forgot password atau verifikasi email, tanpa mengubah kode yang sudah ada. Ini memungkinkan fleksibilitas dalam mengembangkan lebih banyak fitur autentikasi di masa depan. 

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/3-2.gif)

- **Dependency Inversion Principle (Prinsip Inversi Ketergantungan)**

Komponen Account menggunakan hook useAuth dan useSubscription untuk mengakses informasi pengguna dan detail langganan.Komponen Account menggunakan hook useAuth dan useSubscription untuk mengakses informasi pengguna dan detail langganan. 

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/3-3.gif)

- **Interface Segregation Principle (Prinsip Segregasi Antarmuka)**

Prinsip Interface Segregation Principle mengatakan bahwa klien tidak boleh dipaksa untuk bergantung pada antarmuka yang tidak mereka gunakan. Dalam hal ini, saya telah memisahkan antarmuka untuk tiga halaman yang berbeda: index.tsx sebagai halaman utama (homepage), login.tsx sebagai halaman login, dan account.tsx sebagai halaman akun.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/3-4.gif)

- **Dependency Inversion Principle (Prinsip Inversi Ketergantungan)**

Komponen Login menggunakan hook useAuth untuk mengakses fungsi signIn dan signUp. Ini menunjukkan penggunaan abstraksi melalui hook, di mana implementasi sebenarnya dari autentikasi tidak terlihat dalam komponen tersebut.
![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/3-5.gif)

# No 4
**Design Pattern**

Pada project kali ini saya menerapkan design pattern singletone. Singleton adalah sebuah design pattern yang memastikan hanya ada satu instance dari suatu kelas yang dapat diakses secara global. Dalam project saya, pola singleton diterapkan pada kelas Firebase. Class Firebase memiliki method static getInstance() yang mengembalikan instance dari kelas Firebase. Constructor class Firebase dideklarasikan sebagai private, sehingga instance class ini tidak dapat dibuat di luar class. Dengan menggunakan Singleton kita dapat memastikan bahwa hanya ada satu instance Firebase yang dibuat dan digunakan dalam aplikasi.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/4.gif)

Selain itu, karena saya menggunakan reactjs saya menggunakan design pattern react component. Hal ini memisahkan interface pengguna di dalam folder component yang dapat digunakan kembali. Sehingga tidak perlu membuat hal yang sama berulang kali.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/4-2.gif)

# No 5
**Konektivitas ke database**
Pada project ini saya menggunakan firebase sebagai database.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/5.gif)

# No 6
**Web Service**

Karena saya menggunakan firebase maka webservice CRUD dan restful api sudah include di dalam firebase.
![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/6.gif)

# No 7
**Graphical User Interface**

Saya interface pengguna dengan menggunakan react typescript atau tsx. Dan ini merupakan tampilan project saya dengan beberapa fitur yang dibuat berdasarkan use case.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/7.gif)
![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/7_2.gif)

# No 8
**HTTP connection melalui GUI**

Saya menggunakan HTTP connection melalui penggunaan Firebase Authentication API pada project kali ini. Dalam fungsi signUp, signIn, dan logOut, saya menggunakan fungsi-fungsi dari Firebase Authentication seperti createUserWithEmailAndPassword, signInWithEmailAndPassword, dan signOut untuk melakukan interaksi dengan server Firebase melalui permintaan HTTP.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/8-2.gif)

Lalu ini merupakan proses saat user login.

![](https://gitlab.com/mfauzan982/netflixclone-tubespbo/-/raw/main/ss-jawaban/8.gif)

# No 9
**Video Youtube**

Link demo project tugas besar melalui YouTube

https://youtu.be/thG66wIHLxw

# No 10
**Machine Learning** 

Saya belum menerapkan machine learning pada program ini.



