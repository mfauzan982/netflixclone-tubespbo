import { Movie } from "@/typings";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import Thumbnail from "./Thumbnail";
import React from "react";
import { DocumentData } from "firebase/firestore";

interface Props {
  title: string;
  movies: Movie[] | DocumentData[];
}
class CategoryRow extends React.Component<Props> {
  rowRef = React.createRef<HTMLDivElement>();
  state = {
    isMoved: false,
  };

  handleClick = (direction: string) => {
    this.setState({ isMoved: true });

    if (this.rowRef.current) {
      const { scrollLeft, clientWidth } = this.rowRef.current;

      const scrollTo =
        direction === "left"
          ? scrollLeft - clientWidth
          : scrollLeft + clientWidth;
      this.rowRef.current.scrollTo({ left: scrollTo, behavior: "smooth" });
    }
  };

  render() {
    const { title, movies } = this.props;
    const { isMoved } = this.state;

    return (
      <div className=" h-40 gap-0.5 md:gap-2 ">
        <h2 className="w-36 cursor-pointer text-lg font-semibold text-[#e5e5e5] transition duration-200 hover:text-white">
          {title}
        </h2>
        <div className="group relative md:-ml-2">
          <BsChevronLeft
            onClick={() => this.handleClick("left")}
            className={`absolute top-0 bottom-0 left-2 z-40 m-auto h-9 w-9 cursor-pointer opacity-0 transition hover:scale-125 group-hover:opacity-100 ${
              !isMoved && "hidden"
            }`}
          />

          <div
            ref={this.rowRef}
            className="flex items-center gap-0.5 overflow-x-scroll md:gap-2.5 md:p-2 scrollbar-hide"
          >
            {movies.map((movie) => (
              <Thumbnail key={movie.id} movie={movie} />
            ))}
          </div>

          <BsChevronRight
            onClick={() => this.handleClick("right")}
            className={`absolute top-0 bottom-0 right-2 z-40 m-auto h-9 w-9 cursor-pointer opacity-0 transition hover:scale-125 group-hover:opacity-100`}
          />
        </div>
      </div>
    );
  }
}

export default CategoryRow;
