import Link from "next/link";
import { AiOutlineBell, AiOutlineSearch } from "react-icons/ai";
import BasicMenu from "./BasicMenu";
import React, { Component } from "react";

interface State {
  isScrolled: boolean;
}

class Header extends Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = {
      isScrolled: false,
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    if (window.scrollY > 0) {
      this.setState({ isScrolled: true });
    } else {
      this.setState({ isScrolled: false });
    }
  };

  render() {
    const { isScrolled } = this.state;

    return (
      <header className={`${isScrolled && "bg-[#141414]"}`}>
        <div className="flex items-center gap-2 md:gap-10">
          <img
            src="/images/logo.png"
            alt="logo"
            width={100}
            height={100}
            className="cursor-pointer object-contain"
          />

          <BasicMenu />

          <ul className="hidden gap-4 md:flex">
            <li className="headerLink">Home</li>
            <li className="headerLink">Tv Shows</li>
            <li className="headerLink">Movie</li>
            <li className="headerLink">New & Popular</li>
            <li className="headerLink">My List</li>
          </ul>
        </div>

        <div className="flex items-center gap-4 text-sm font-light">
          <AiOutlineSearch className="hidden sm:inline h-6 w-6 cursor-pointer" />
          <p className="hidden lg:inline cursor-pointer ">Kids</p>
          <AiOutlineBell className="h-6 w-6 cursor-pointer" />
          <Link href="/account">
            <img
              src="/images/default-blue.png"
              alt="Account"
              className="w-6 h-6 lg:w-10 lg:h-10 rounded-md cursor-pointer"
            />
          </Link>
        </div>
      </header>
    );
  }
}

export default Header;
