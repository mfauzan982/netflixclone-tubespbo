import { Button, Menu, MenuItem } from "@mui/material";
import React from "react";

// For Responsive Header
class BasicMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = (event: any) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div className="md:!hidden">
        <Button
          id="basic-button"
          aria-controls={open ? "basic-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
          onClick={this.handleClick}
          className="!capitalize !text-white"
        >
          Browse
        </Button>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
          className="menu"
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={this.handleClose}>Home</MenuItem>
          <MenuItem onClick={this.handleClose}>TV Shows</MenuItem>
          <MenuItem onClick={this.handleClose}>Movies</MenuItem>
          <MenuItem onClick={this.handleClose}>New & Popular</MenuItem>
          <MenuItem onClick={this.handleClose}>My List</MenuItem>
        </Menu>
      </div>
    );
  }
}

export default BasicMenu;
