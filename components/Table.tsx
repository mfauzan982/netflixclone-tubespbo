import React, { Component } from "react";
import { Product } from "@stripe/firestore-stripe-payments";
import { AiOutlineCheck } from "react-icons/ai";

interface Props {
  products: Product[];
  selectedPlan: Product | null;
}

class Table extends Component<Props> {
  render() {
    const { products, selectedPlan } = this.props;

    return (
      <table>
        <tbody className="divide-y divide-[gray]">
          <tr className="tableRow">
            <td className="tableDataTitle">Monthly price</td>
            {products.map((product) => (
              <td
                key={product.id}
                className={`tableDataFeature ${
                  selectedPlan?.id === product.id
                    ? "text-[#e50914]"
                    : "text-[gray]"
                }`}
              >
                IDR{product.prices[0].unit_amount! / 100}
              </td>
            ))}
          </tr>

          <tr className="tableRow">
            <td className="tableDataTitle">Video Quality</td>
            {products.map((product) => (
              <td
                key={product.id}
                className={`tableDataFeature ${
                  selectedPlan?.id === product.id
                    ? "text-[#e50914]"
                    : "text-[gray]"
                }`}
              >
                {product.metadata.videoQuality}
              </td>
            ))}
          </tr>
          <tr className="tableRow">
            <td className="tableDataTitle">Resolution</td>
            {products.map((product) => (
              <td
                className={`tableDataFeature ${
                  selectedPlan?.id === product.id
                    ? "text-[#E50914]"
                    : "text-[gray]"
                }`}
                key={product.id}
              >
                {product.metadata.resolution}
              </td>
            ))}
          </tr>

          <tr className="tableRow">
            <td className="tableDataTitle">
              Watch on your TV, computer, mobile phone and tablet
            </td>
            {products.map((product) => (
              <td
                className={`tableDataFeature ${
                  selectedPlan?.id === product.id
                    ? "text-[#E50914]"
                    : "text-[gray]"
                }`}
                key={product.id}
              >
                {product.metadata.portability === "true" && (
                  <AiOutlineCheck className="inline-block h-8 w-8" />
                )}
              </td>
            ))}
          </tr>
        </tbody>
      </table>
    );
  }
}

export default Table;
