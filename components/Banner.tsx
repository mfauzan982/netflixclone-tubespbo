import Image from "next/legacy/image";
import { Movie } from "@/typings";
import { useEffect, useState } from "react";
import { baseUrl } from "@/constants/movie";
import { FaPlay } from "react-icons/fa";
import { AiOutlineInfoCircle } from "react-icons/ai";
import { useRecoilState } from "recoil";
import { modalState, movieState } from "@/atoms/modalAtom";

interface Props {
  netflixOriginals: Movie[];
}

function Banner({ netflixOriginals }: Props) {
  const [movie, setMovie] = useState<Movie | null>(null);
  const [showModal, setShowModal] = useRecoilState(modalState);
  const [currentMovie, setCurrentMovie] = useRecoilState(movieState);

  useEffect(() => {
    setMovie(
      netflixOriginals[Math.floor(Math.random() * netflixOriginals.length)]
    );
  });

  return (
    <div className="flex flex-col gap-2 py-16 md:gap-4 lg:justify-end ">
      <div className="absolute top-0 left-0 -z-10 h-[95vh] w-screen object-cover ">
        <Image
          src={`${baseUrl}${movie?.backdrop_path || movie?.backdrop_path}`}
          alt={""}
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div className="lg:mt-32">
        <h1 className="text-2xl lg:text-5xl md:text-4xl font-bold">
          {movie?.title || movie?.name || movie?.original_name}
        </h1>
        <p className="max-w-xs text-xs md:max-w-lg md:text-lg lg:max-w-2xl lg:text-2xl ">
          {movie?.overview}
        </p>

        <div className="flex flex-row gap-3 ">
          <button className="bannerButton bg-white text-black">
            <FaPlay className="h-2 w-2 text-black md:h-5 md:w-5" />
            Play
          </button>
          <button
            className="bannerButton bg-[gray]/70"
            onClick={() => {
              setCurrentMovie(movie);
              setShowModal(true);
            }}
          >
            <AiOutlineInfoCircle className="h-5 w-5 md:h-8 md:w-8" />
            More Info
          </button>
        </div>
      </div>
    </div>
  );
}

export default Banner;
