import { initializeApp, getApp, getApps, FirebaseApp } from "firebase/app";
import { getFirestore, Firestore } from "firebase/firestore";
import { getAuth, Auth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyCwyCA-i0p18_GtfuCsUbNOxtSZigbqYRU",
  authDomain: "netflix-clone-tubes.firebaseapp.com",
  projectId: "netflix-clone-tubes",
  storageBucket: "netflix-clone-tubes.appspot.com",
  messagingSenderId: "474700568596",
  appId: "1:474700568596:web:878c559c4d030486782f01",
};

class Firebase {
  private static instance: Firebase | null = null;
  public app: FirebaseApp;
  public db: Firestore;
  public auth: Auth;

  private constructor() {
    this.app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
    this.db = getFirestore();
    this.auth = getAuth();
  }

  public static getInstance(): Firebase {
    if (!Firebase.instance) {
      Firebase.instance = new Firebase();
    }
    return Firebase.instance;
  }
}

const firebaseInstance = Firebase.getInstance();
export const app = firebaseInstance.app;
const auth = firebaseInstance.auth;
const db = firebaseInstance.db;
export { auth, db };
export default Firebase;
