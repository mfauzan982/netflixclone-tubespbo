import { AuthProvider } from "@/hooks/useAuth";
import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { RecoilRoot } from "recoil";
import React from "react";

class App extends React.Component<AppProps> {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <RecoilRoot>
        <AuthProvider>
          <Component {...pageProps} />
        </AuthProvider>
      </RecoilRoot>
    );
  }
}

export default App;
