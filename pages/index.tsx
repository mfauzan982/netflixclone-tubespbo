import Head from "next/head";
import Header from "@/components/Header";
import Banner from "@/components/Banner";
import request from "@/utils/request";
import { Movie } from "@/typings";
import CategoryRow from "@/components/CategoryRow";
import useAuth from "@/hooks/useAuth";
import { useRecoilValue } from "recoil";
import { modalState, movieState } from "@/atoms/modalAtom";
import Modal from "@/components/Modal";
import Plans from "@/components/Plans";
import { Product, getProducts } from "@stripe/firestore-stripe-payments";
import payments from "@/lib/stripe";
import useSubscription from "@/hooks/useSubscription";
import useList from "@/hooks/useList";

interface Props {
  netflixOriginals: Movie[];
  trendingNow: Movie[];
  topRated: Movie[];
  actionMovies: Movie[];
  comedyMovies: Movie[];
  horrorMovies: Movie[];
  romanceMovies: Movie[];
  documentaries: Movie[];
  products: Product[];
}
export default function Home({
  netflixOriginals,
  trendingNow,
  topRated,
  actionMovies,
  comedyMovies,
  horrorMovies,
  romanceMovies,
  documentaries,
  products,
}: Props) {
  const { logOut, loading, user } = useAuth();
  const showModal = useRecoilValue(modalState);
  const subscription = useSubscription(user);
  const movie = useRecoilValue(movieState);
  const list = useList(user?.uid);

  if (loading || subscription === null) return null;

  if (!subscription) return <Plans products={products} />;

  return (
    <div className="relative h-screen bg-gradient-to-b from-gray-900/10 to-[#010511] lg:h-[140vh]">
      <Head>
        <title>Netflix</title>
        <link rel="icon" href="/small-logo.png" />
      </Head>
      <Header />
      <main className="relative pl-4 pb-24 lg:gap-24 lg:pl-16">
        <Banner netflixOriginals={netflixOriginals} />
        <section className="md:space-y-24">
          <CategoryRow title="Trending Now" movies={trendingNow} />
          <CategoryRow title="Top Rated" movies={topRated} />
          <CategoryRow title="Action Thrillers" movies={actionMovies} />
          {/* My List Component */}
          {list.length > 0 && <CategoryRow title="My List" movies={list} />}

          <CategoryRow title="Comedies" movies={comedyMovies} />
          <CategoryRow title="Scary Movies" movies={horrorMovies} />
          <CategoryRow title="Romance Movies" movies={romanceMovies} />
          <CategoryRow title="Documentaries" movies={documentaries} />
        </section>
      </main>
      {showModal && <Modal />}
    </div>
  );
}

export const getServerSideProps = async () => {
  const [
    netflixOriginals,
    trendingNow,
    topRated,
    actionMovies,
    comedyMovies,
    horrorMovies,
    romanceMovies,
    documentaries,
  ] = await Promise.all([
    fetch(request.fetchNetflixOriginals).then((res) => res.json()),
    fetch(request.fetchTrending).then((res) => res.json()),
    fetch(request.fetchTopRated).then((res) => res.json()),
    fetch(request.fetchActionMovies).then((res) => res.json()),
    fetch(request.fetchComedyMovies).then((res) => res.json()),
    fetch(request.fetchHorrorMovies).then((res) => res.json()),
    fetch(request.fetchRomanceMovies).then((res) => res.json()),
    fetch(request.fetchDocumentaries).then((res) => res.json()),
  ]);

  const products = await getProducts(payments, {
    includePrices: true,
    activeOnly: true,
  })
    .then((res) => res)
    .catch((err) => console.log(err.message));

  return {
    props: {
      netflixOriginals: netflixOriginals.results,
      trendingNow: trendingNow.results,
      topRated: topRated.results,
      actionMovies: actionMovies.results,
      comedyMovies: comedyMovies.results,
      horrorMovies: horrorMovies.results,
      romanceMovies: romanceMovies.results,
      documentaries: documentaries.results,
      products,
    },
  };
};
